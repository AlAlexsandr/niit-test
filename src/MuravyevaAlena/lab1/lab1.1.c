/*
 ============================================================================
 Name        : lab1.1.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : Program calculates norms weight body and deviation
 ============================================================================*/

#include <stdio.h>


int main()
{
   char sex=0;
   int growth=0;
   int weight=0;

   setlinebuf(stdout);

start:
   printf("Hello, this is weight calculator.\n Enter sex: \n");
   scanf("%c", &sex);
   printf("\nEnter growth: \n");
   scanf("%d",&growth);
   printf("\nEnter weight: \n");
   scanf("%d",&weight);

   printf("\nYou entered growth=%d and weight=%d\n", growth, weight);

   int result = growth - weight;

   if(sex=='m')
   {
      printf("Result: ");
      if(result > 100)
         printf("lack of weight\n");
      else if (result==100)
         printf("weight is norm\n");
      else if(result <100)
         printf("weight excess\n");
   }
   else if(sex=='w')
   {
      printf("Result: ");
      if(result>110)
         printf("lack of weight\n");
      else if (result==110)
         printf("weight is norm\n");
      else if(result<110)
         printf("weight excess\n");
   }
   else
   {
      printf("Error, please enter correct sex\n");
      goto start;
   }
   return 0;
}


